
import sys
sys.path.append('..')

from WebScrapingSession import *

from bs4 import BeautifulSoup
from bs4.element import Tag

__CONFIG_PATH = 'config.json'
__SESSION_DESCRIPTION = 'Craigslist session'

class CraigslistConfig(WebScrapingConfig):
    def __init__(self, config_path: str):
        super(CraigslistConfig, self).__init__(config_path=config_path)


class CraigslistWebScrapingSession(WebScrapingSession):
    def __init__(self,
                 config_path: str,
                 batch_comment: str):
        super(CraigslistWebScrapingSession, self).__init__(config_path=config_path,
                                                           batch_comment=batch_comment)

    @staticmethod
    def try_to_get_tag_class(tag: Tag):
        ret = ''
        try:
            ret = tag['class'][0]
        except:
            pass

        return ret

    @staticmethod
    def try_to_get_tag_contents(tag: Tag):
        ret = ''
        try:
            # # ret = tag.contents[0]
            # contents = []
            # for content in tag.contents:
            #     contents.append(content)
            #     # if type(content) == str:
            #     #     contents.append(content)
            #     # else:
            #     #     contents.append(content.text)
            #
            # ret = ''.join(contents)
            ret = tag.text
        except:
            pass

        return ret

    @staticmethod
    def get_tag_class_and_content_as_dict(tag: Tag):
        if not tag:
            return

        key = Utility.try_to_get_tag_class(tag)
        val = Utility.try_to_get_tag_contents(tag)

        if not key and not val:
            return

        if not key:
            key = val

        if not val:
            val = key

        m = {key: val}

        return m

    def process_page(self, url: str):
        if not url:
            return

        self.logger.info('Processing: {0}'.format(url))

class SearchResultsPage(object):
    def __init__(self,
                 soup: BeautifulSoup,
                 session: CraigslistWebScrapingSession,
                 page_url: str):
        self.soup = soup
        self.session = session
        self.page_url = page_url

    def get_all_result_items(self):
        if not self.soup:
            return

        results = self.soup.find('ul', class_='rows')
        if not results:
            raise TagNotFoundException('No tag found for ul tags with class = rows')

        result_items = results.find_all('li', class_='result-row')

        search_result_items = []

        for row in result_items:
            search_result_items.append(SearchResultItem(row))

        return search_result_items

    def get_existing_urls(self):
        conn = CraigslistDatabase.get_conn()
        cur = conn.cursor()

        existing_urls = []
        try:
            cur.execute('SELECT DISTINCT Url FROM SearchResult')
            for row in cur.fetchall():
                existing_urls.append(str(row[0]).strip().lower())
        except sqlite3.Error as e:
            print('Unable to get URLs: \n{0}'.format(e))

        return existing_urls

    # TODO: Check to make sure records are not duplicated
    def insert_records(self, records: []):
        if not records:
            return

        conn = self.get_conn()
        cur = conn.cursor()

        existing_urls = self.get_existing_urls()

        for result in records:
            try:
                if None != result.href and str(result.href).strip().lower() in existing_urls:
                    continue
                cur.execute("""INSERT INTO SearchResult (
                                        BatchId
                                        ,Name
                                        ,Url
                                        ,Timestamp
                                        ,Price
                                    )
                                    VALUES (
                                        ?, ?, ?, ?, ?
                                    )
                                """, (self.session.batch_id,
                                      result.name,
                                      result.href,
                                      result.timestamp,
                                      result.price))
                conn.commit()
            except sqlite3.Error as e:
                conn.rollback()
                print('Failed to insert row: {0}'.format(e))

    def process_search_results_page(self):
        records = self.get_all_result_items()
        self.insert_records(records)

    def get_next_page_url(self):
        url = ''
        try:
            try:
                url = self.soup.find('div', class_='paginator buttongroup firstpage').find(class_='buttons').find(
                    class_='button next').attrs['href']
            except:
                url = self.soup.find('div', class_='paginator buttongroup').find(class_='buttons').find(
                    class_='button next').attrs['href']
            url = urllib.parse.urljoin(self.page_url, url)
        except:
            pass

        return url

class SearchResultItem(object):
    def __init__(self, tag: Tag):
        self.tag = tag
        self._result_info = self.__get_result_info()

        self.name = self.__get_name()
        self.price = self.__get_price()
        self.timestamp = self.__get_timestamp()
        self.href = self.__get_href()
        self.metadata = self.__get_metadata()

    def __get_result_info(self):
        try:
            ret = self.tag.find(class_='result-info')

            if not ret:
                raise TagNotFoundException
        except TagNotFoundException:
            print('Failed to find result-info tag')

        return ret

    def __get_name(self):
        try:
            ret = self.tag.find(class_='result-title hdrlnk').text

            if not ret:
                return ''

            return ret
        except:
            return ''

    def __get_price(self):
        # price_str = ''
        try:
            price_str = self.tag.find('span', class_='result-price').text
        except:
            return -1

        import re

        if not price_str:
            return -1

        ret = re.sub('[^\d]', '', price_str)

        if not ret:
            return -1

        return ret

    def __get_timestamp(self):
        try:
            ret = self._result_info.find('time', class_='result-date').attrs['datetime']
            if not ret:
                return ''
            return ret
        except:
            return ''

    def __get_href(self):
        try:
            ret = self._result_info.find(class_='result-title hdrlnk').attrs['href']
            if not ret:
                return ''
            return ret
        except:
            return ''

    def __get_metadata(self):
        m = {}
        listing_metadata = self._result_info.find_all('span')

        if not listing_metadata:
            return

        # TODO: only getting first metadata item for each item
        for metadata_item in listing_metadata:
            key = None
            value = None
            try:
                if metadata_item.attrs['class']:
                    key = metadata_item.attrs['class'][0]
                if metadata_item.contents:
                    value = metadata_item.contents[0]
            except:
                pass

            m[key] = value

        if not m:
            m = {''}

        return m